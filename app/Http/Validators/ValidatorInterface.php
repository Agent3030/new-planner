<?php

namespace App\Http\Validators;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;

interface ValidatorInterface
{
    public function validated() : bool;

    public function getErrors() : MessageBag;

    public function validate(Request $request)  : void;

}
