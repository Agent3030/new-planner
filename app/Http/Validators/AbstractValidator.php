<?php

namespace App\Http\Validators;

use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Http\Request;
use Illuminate\Validation\Factory as ValidationFactory;

abstract class AbstractValidator implements ValidatorInterface
{
    protected $validationFactory;
    public $rules =[];
    protected $request;
    protected $errors =[];
    protected $validated;

    /**
     * @param $validatorFactory
     */
    public function __construct(ValidationFactory $validationFactory, Request $request)
    {
        $this->validationFactory = $validationFactory;
    }
    public function validate(Request $request) : void
    {
        $validator = $this->validationFactory->make($request->all(),$this->rules);



        if($validator->fails()){
            $this->errors = $validator->messages();
            $this->validated = false;

        } else {
            $this->validated=true;
        }
    }

    public function validated() : bool
    {

        return $this->validated;
    }

    public function getErrors() : MessageBag
    {
        return $this->errors;
    }


}
