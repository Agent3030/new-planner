<?php

namespace App\Http\Validators;

class SignUpValidator extends AbstractValidator
{
    public $rules = [
        'name' => 'required|unique:users|max:255',
        'email' => 'required|email',
        'password' => 'required',
        'contact_phone'=>'required',
        'user_type_id'=>'required|numeric'
    ];

}
