<?php

namespace App\Http\Validators;

class TokenValidator extends AbstractValidator
{
    public $rules = [
        'email' => 'required|email',
        'password' => 'required',
        'device_name'=>'required'
    ];

}
