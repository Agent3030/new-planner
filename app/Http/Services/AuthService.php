<?php

namespace App\Http\Services;



use App\Http\Services\Responses\ServiceResponseInterface;
use App\Http\Services\Responses\SignupServiceFailedResponse;
use App\Http\Services\Responses\SignupServiceSuccessResponse;
use App\Http\Validators\SignUpValidator;
use App\Http\Validators\ValidatorInterface;
use App\Models\User;
use http\Client\Request;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

abstract class AuthService
{
    protected $model;
    public $rules =[];
    protected $validator;
    /**
     * @var ServiceResponseInterface
     */
    protected $successResponse;
    /**
     * @var ServiceResponseInterface
     */
    protected $failedResponse;
    protected $messages;
    public function __construct(User $model,ValidatorInterface $validator, ServiceResponseInterface $successResponse, ServiceResponseInterface $failedResponse)
    {
        $this->model = $model;
        $this->validator= $validator;
        $this->successResponse= $successResponse;
        $this->failedResponse = $failedResponse;
    }


    public function getSuccessResponse () : ServiceResponseInterface
    {
        return $this->successResponse;
    }
    public function getFailedResponse() : ServiceResponseInterface
    {
        return $this->failedResponse;
    }
}
