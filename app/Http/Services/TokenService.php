<?php

namespace App\Http\Services;

use App\Http\Services\Responses\SignupServiceFailedResponse;
use App\Http\Services\Responses\SignupServiceSuccessResponse;
use App\Http\Services\Responses\TokenServiceFailedResponse;
use App\Http\Services\Responses\TokenServiceSuccessResponse;
use App\Http\Validators\SignUpValidator;
use App\Http\Validators\TokenValidator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use TheSeer\Tokenizer\Token;

class TokenService extends AuthService
{

    public function __construct(User $model,TokenValidator $validator, TokenServiceSuccessResponse  $response, TokenServiceFailedResponse $failedResponse)
    {
        parent::__construct($model,$validator, $response,  $failedResponse);
    }

    public function login(\Illuminate\Http\Request $request)
    {
        $this->validator->validate($request);

        if($this->validator->validated()){
            $model =  $this->model->where('email', $request->email)->first();

            if ( $model ||  Hash::check($request->password, $model ?$model->password :null)) {
                Auth::login($model, TRUE);
                $this->successResponse->setToken($this->model->createToken($request->device_name)->plainTextToken);
            } else {
                $this->failedResponse->setMessage( ['email' => 'The provided credentials are incorrect.']);

                throw ValidationException::withMessages([
                    'email' => ['The provided credentials are incorrect.'],
                ]);
            }

        } else{
            $this->failedResponse->setMessage($this->validator->getErrors());

            throw ValidationException::withMessages($this->validator->getErrors()->getMessages());
        }
    }

}
