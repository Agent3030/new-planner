<?php

namespace App\Http\Services;

use App\Http\Services\Responses\SignupServiceFailedResponse;
use App\Http\Services\Responses\SignupServiceSuccessResponse;
use App\Http\Validators\SignUpValidator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class SignupService extends AuthService
{

    public function __construct(User $model,SignUpValidator $validator, SignupServiceSuccessResponse $response, SignupServiceFailedResponse $failedResponse)
    {
        parent::__construct($model,$validator, $response,  $failedResponse);
    }

    public function signup(\Illuminate\Http\Request $request)
    {
        $this->validator->validate($request);

        if($this->validator->validated()){
            $signupData = $request->all();
            $hashPassword = Hash::make($signupData['password']);
            $signupData['password']=$hashPassword;
            $this->model->create($signupData);

        } else{
            $this->failedResponse->setMessage($this->validator->getErrors());

            throw ValidationException::withMessages($this->validator->getErrors()->getMessages());
        }
    }

}
