<?php

namespace App\Http\Services\Responses;

class AbstractServiceResponse implements ServiceResponseInterface
{
    public $status;
    public $code;
    public $message;

}
