<?php

namespace App\Http\Services\Responses;

class TokenServiceSuccessResponse implements ServiceResponseInterface
{
    public $status ='success';
    public $message = 'User Successfully Logined';
    public $token;

    /**
     * @param mixed $token
     */
    public function setToken($token): void
    {
        $this->token = $token;
    }


}
