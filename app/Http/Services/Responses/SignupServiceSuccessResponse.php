<?php

namespace App\Http\Services\Responses;

class SignupServiceSuccessResponse implements ServiceResponseInterface
{
    public $status ='success';
    public $message = 'User Successfully Create';

}
