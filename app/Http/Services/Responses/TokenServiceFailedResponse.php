<?php

namespace App\Http\Services\Responses;

class TokenServiceFailedResponse implements ServiceResponseInterface
{
    public $status ='failed';
    public $type;
    public $message;

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }



}
