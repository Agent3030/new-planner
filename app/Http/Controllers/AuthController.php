<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignUpRequest;
use App\Http\Services\SignupService;
use App\Http\Services\TokenService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    protected $signupService;
    protected $tokenService;

    /**
     * @param $service
     */
    public function __construct(SignupService $signupService, TokenService $tokenService)
    {
        $this->signupService = $signupService;
        $this->tokenService = $tokenService;
    }

    public function register(Request $request)
    {
        try {
            $this->signupService->signup($request);
            $result = $this->signupService->getSuccessResponse();

        } catch (ValidationException $e)
        {
            $result=  $this->signupService->getFailedResponse();
            $result->setType('Validation');
        } catch (\Throwable$e)
        {
            $result=  $this->signupService->getFailedResponse();
            $result->setType('System');
            $result->setMessage($e->getMessage());
        }

        return response()->json($result);

    }
    public function hello()
    {
        dd('hello');
    }
    public function token(Request $request)
    {
        try {
            $this->tokenService->login($request);
            $result = $this->tokenService->getSuccessResponse();

        } catch (ValidationException $e)
        {
            $result=  $this->tokenService->getFailedResponse();
            $result->setType('Validation');
        } catch (\Throwable$e)
        {
            $result=  $this->tokenService->getFailedResponse();
            $result->setType('System');
            $result->setMessage($e->getMessage());
        }

        return response()->json($result);
    }
}
