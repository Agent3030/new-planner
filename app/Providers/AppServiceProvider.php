<?php

namespace App\Providers;

use App\Http\Controllers\AuthController;
use App\Http\Services\Responses\SignupServiceFailedResponse;
use App\Http\Services\Responses\SignupServiceSuccessResponse;
use App\Http\Services\Responses\TokenServiceFailedResponse;
use App\Http\Services\Responses\TokenServiceSuccessResponse;
use App\Http\Services\SignupService;
use App\Http\Services\TokenService;
use App\Http\Validators\SignUpValidator;
use App\Http\Validators\TokenValidator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public $bindings = [
        SignUpValidator::class => SignUpValidator::class,
        SignupService::class =>SignupService::class,
        SignupServiceSuccessResponse::class=> SignupServiceSuccessResponse::class,
        SignupServiceFailedResponse::class=> SignupServiceFailedResponse::class,
        AuthController::class=>AuthController::class,
        TokenValidator::class=>TokenValidator::class,
        TokenService::class=>TokenService::class,
        TokenServiceSuccessResponse::class=>TokenServiceSuccessResponse::class,
        TokenServiceFailedResponse::class=>TokenServiceFailedResponse::class
    ];
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
