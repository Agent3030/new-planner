<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'root',
            'email' => 'root'.'@gmail.com',
            'password' => Hash::make('root'),
            'contact_phone'=> '+380675472564',
            'user_type_id'=> '0'
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'test artist 1',
            'email' => 'TestArtist1'.'@gmail.com',
            'password' => Hash::make('TestArtist1'),
            'contact_phone'=> '+380111111111',
            'user_type_id'=> '1'
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'test venue 1',
            'email' => 'TestVenue1'.'@gmail.com',
            'password' => Hash::make('TestVenue1'),
            'contact_phone'=> '+3802222222222',
            'user_type_id'=> '2'
        ]);
        \Illuminate\Support\Facades\DB::table('users')->insert([
            'name' => 'test eventer 1',
            'email' => 'TestEventer1'.'@gmail.com',
            'password' => Hash::make('TestEventer1'),
            'contact_phone'=> '+380333333333',
            'user_type_id'=> '3'
        ]);


    }
}
